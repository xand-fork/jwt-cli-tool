## JWT CLI TOOL
To install tool, make sure npm is installed and run
```
$ sudo npm install -g 
```
To use the tool, run
```
$ jwt {secret} [options]
```

### Options
`-e {email address}` or `--email {email address}`: To associate an email address with the token
<!---
`-a {audience}` or `--audience {audience}`: Specify audience

`-d {domain}` or `--domain {domain}`: Specify domain
--->
`-x {expiration}` or `--expires {expiration}`: 'Time to live for token. 
1s, 3w, 2y, etc. Defaults to `1y` (1 year).'